package application.database;

import application.entity.Bill;

import java.sql.SQLException;
import java.util.Collection;

public interface BillingDAO {

    Collection<Bill> getAllBills() throws SQLException;

    Collection<Bill> getBillsByCustomerId(Long serviceId) throws SQLException;

    void addBill(Bill bill) throws SQLException;

    void update(Bill bill) throws SQLException;
}