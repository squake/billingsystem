package application.controller;

import application.model.TariffModel;
import application.service.TariffServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
public class TariffController {

    @Autowired
    private TariffServiceImpl tariffService;

    @GetMapping(value = "/tariffs")
    public Collection<TariffModel> getAvailableTariffModels() {
        return tariffService.getAvailableTariffModels();
    }

    @GetMapping(value = "/tariffs/all")
    public Collection<TariffModel> getAllTariffModels() {
        return tariffService.getAllTariffModels();
    }

    @GetMapping(value = "/tariff/info")
    public TariffModel getTariffModelById(@RequestParam(name = "id", required = true) Long id) {
        return tariffService.getTariffModelById(id);
    }

}
