package application.database;

import application.entity.Country;

import java.sql.SQLException;
import java.util.Collection;

public interface CountryDAO {
    void addCountry(Country country) throws SQLException;

    Country getCountryById(Long countryId) throws SQLException;

    Country getCountryByCountryCode(Integer countryCode) throws SQLException;

    Collection<Country> getAllCountries() throws SQLException;
}
