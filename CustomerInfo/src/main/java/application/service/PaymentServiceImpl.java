


package application.service;

import application.database.CustomerInfoFactory;
import application.entity.Customer;
import application.entity.Payment;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

@Service
public class PaymentServiceImpl implements PaymentService {

    @Override
    public Collection<Payment> getAllPayments() {
        try {
            return CustomerInfoFactory.getInstance().getPaymentDAO().getAllPayments();
        } catch (SQLException exc) {
            return null;
        }
    }

    @Override
    public void save(Payment payment) {
        try {
            CustomerInfoFactory.getInstance().getPaymentDAO().addPayment(payment);
        } catch (SQLException exc) {
            throw new RuntimeException(exc.getMessage(), exc);
        }
    }

    @Override
    public List<Payment> getPaymentsByCustomer(Customer customer) {
        try {
            return CustomerInfoFactory.getInstance().getPaymentDAO().getPaymentsByCustomer(customer);
        } catch (SQLException exc) {
            System.out.println(exc);
            return null;
        }
    }

}


