package application.entity;

import application.enums.ServiceType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@ToString(exclude = {"customer"})
@EqualsAndHashCode(exclude = {"customer"})
@NoArgsConstructor
@Table(name = "bills")
public class Bill {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @Column(name = "description")
    private String description;

    @CreationTimestamp
    @Column(name = "creation_date")
    private Date creationDate;

    @NonNull
    @Column(name = "currency_code")
    private String currencyCode;

    @NonNull
    @Column(name = "status")
    private String status;

    @NonNull
    @Column(name = "subtotal")
    private Double subtotal;

    @NonNull
    @Column(name = "discount")
    private Double discount;

    @NonNull
    @Column(name = "total")
    private Double total;

    @NonNull
    @Column(name = "service_type")
    private ServiceType serviceType;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "service_id", nullable = false)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    private Customer customer;

    public Bill(@NonNull String description, Date creationDate, @NonNull String currencyCode, @NonNull String status, @NonNull Double subtotal, @NonNull Double discount, @NonNull Double total, @NonNull ServiceType serviceType, Customer customer) {
        this.description = description;
        this.creationDate = creationDate;
        this.currencyCode = currencyCode;
        this.status = status;
        this.subtotal = subtotal;
        this.discount = discount;
        this.total = total;
        this.serviceType = serviceType;
        this.customer = customer;
    }
}