package application.service;

import application.database.CustomerInfoFactory;
import application.entity.Country;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.Collection;

@Service
public class CountryServiceImpl implements CountryService {

    @Override
    public Collection<Country> getAllCountries() {
        try {
            return CustomerInfoFactory.getInstance().getCountryDAO().getAllCountries();
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public Country getCountryById(Long countryId) {
        try {
            return CustomerInfoFactory.getInstance().getCountryDAO().getCountryById(countryId);
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public Country getCountryByCountryCode(Integer countryCode) {
        try {
            return CustomerInfoFactory.getInstance().getCountryDAO().getCountryByCountryCode(countryCode);
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public void save(Country country) {
        try {
            CustomerInfoFactory.getInstance().getCountryDAO().addCountry(country);
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
