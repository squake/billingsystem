package application.database;

public class CustomerInfoFactory {

    private static CountryDAO countryDAO = null;
    private static RegionDAO regionDAO = null;
    private static CustomerDAO customerDAO = null;
    private static TariffDAO tariffDAO = null;
    private static PaymentDAO paymentDAO = null;
    private static CustomerInfoFactory instance = null;

    public static synchronized CustomerInfoFactory getInstance() {
        if (instance == null) {
            instance = new CustomerInfoFactory();
        }
        return instance;
    }

    public CountryDAO getCountryDAO() {
        if (countryDAO == null) {
            countryDAO = new CountryDAOImpl();
        }
        return countryDAO;
    }

    public RegionDAO getRegionDAO() {
        if (regionDAO == null) {
            regionDAO = new RegionDAOImpl();
        }
        return regionDAO;
    }

    public CustomerDAO getCustomerDAO() {
        if (customerDAO == null) {
            customerDAO = new CustomerDAOImpl();
        }
        return customerDAO;
    }

    public TariffDAO getTariffDAO() {
        if (tariffDAO == null) {
            tariffDAO = new TariffDAOImpl();
        }
        return tariffDAO;
    }

    public PaymentDAO getPaymentDAO() {
        if(paymentDAO == null) {
            paymentDAO = new PaymentDAOImpl();
        }
        return paymentDAO;
    }
}
