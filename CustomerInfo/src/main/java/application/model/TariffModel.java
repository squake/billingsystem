package application.model;

import application.entity.Tariff;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TariffModel {
    private String name;
    private Double monthlyPrice;
    private Long freeSeconds;
    private Long freeSMS;
    private Double freeMegabytes;
    private Double pricePerMinute;
    private Double pricePerSMS;
    private Double pricePerMegabyte;
    private Double discount;

    public TariffModel(@NonNull Tariff tariff) {
        name = tariff.getName();
        monthlyPrice = tariff.getMonthlyPrice();
        freeSeconds = tariff.getFreeSeconds();
        freeSMS = tariff.getFreeSMS();
        freeMegabytes = tariff.getFreeMegabytes();
        pricePerMinute = tariff.getPricePerMinute();
        pricePerSMS = tariff.getPricePerSMS();
        pricePerMegabyte = tariff.getPricePerMegabyte();
        discount = tariff.getDiscount();
    }
}
