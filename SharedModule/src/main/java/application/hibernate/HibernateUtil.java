package application.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
    private static final SessionFactory sessionFactory;

    static {
        try {
            Configuration configuration = new Configuration().configure();
            configuration.addAnnotatedClass(Class.forName("application.entity.Country"));
            configuration.addAnnotatedClass(Class.forName("application.entity.Region"));
            configuration.addAnnotatedClass(Class.forName("application.entity.Tariff"));
            configuration.addAnnotatedClass(Class.forName("application.entity.Customer"));
            configuration.addAnnotatedClass(Class.forName("application.entity.Payment"));
            configuration.addAnnotatedClass(Class.forName("application.entity.Bill"));
            configuration.addAnnotatedClass(Class.forName("application.entity.CallDetailRecord"));
            configuration.addAnnotatedClass(Class.forName("application.entity.VoiceCallDetailRecord"));
            configuration.addAnnotatedClass(Class.forName("application.entity.SMSCallDetailRecord"));
            configuration.addAnnotatedClass(Class.forName("application.entity.TrafficCallDetailRecord"));
            StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
            sessionFactory = configuration.buildSessionFactory(builder.build());
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static Session getSession() {
        return sessionFactory.openSession();
    }
}
