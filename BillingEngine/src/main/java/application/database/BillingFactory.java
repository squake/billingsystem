package application.database;

public class BillingFactory {

    private static BillingDAO billingDAO = null;
    private static BillingFactory instance = null;

    public static synchronized BillingFactory getInstance() {
        if (instance == null) {
            instance = new BillingFactory();
        }
        return instance;
    }

    public BillingDAO getBillingDAO() {
        if (billingDAO == null) {
            billingDAO = new BillingDAOImpl();
        }
        return billingDAO;
    }
}