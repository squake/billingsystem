package application.initialization;

import application.entity.*;
import application.service.CountryServiceImpl;
import application.service.RegionServiceImpl;
import application.service.TariffServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class CustomerInfoUtils {
    @Autowired
    RegionServiceImpl regionService;
    @Autowired
    CountryServiceImpl countryService;
    @Autowired
    TariffServiceImpl tariffService;

    protected List<Country> createCountries() {
        return new ArrayList<Country>() {{
            add(new Country(7, "RU", "Russia", null));
        }};
    }

    protected List<Region> createRegions() {
        return new ArrayList<Region>() {{
            add(new Region(countryService.getCountryByCountryCode(7), "MSK", "Moscow"));
            add(new Region(countryService.getCountryByCountryCode(7), "MR", "Moscow region"));
        }};
    }

    protected List<Payment> createPayments() {
        List<Customer> customers = createCustomers();
        return new ArrayList<Payment>() {{
            add(new Payment(customers.get(0), 0., 0.));
            add(new Payment(customers.get(0), 15., 15.));
            add(new Payment(customers.get(0), 30., 45.));
            add(new Payment(customers.get(0), 40., 85.));
            add(new Payment(customers.get(0), 15., 100.));
            add(new Payment(customers.get(0), 15., 115.));
            add(new Payment(customers.get(0), 10., 125.));
            add(new Payment(customers.get(0), 20., 145.));
            add(new Payment(customers.get(1), 100., 130.4));
        }};
    }

    protected List<Tariff> createTariffs() {
        return new ArrayList<Tariff>() {{
           add(new Tariff("Harmony S",
                   300., 6000L, 100L, 3072.,
                   1.2, 0.6, 0.0390625, 0.));
            add(new Tariff("Harmony M",
                    440., 12000L, 200L, 5120.,
                    1.1, 0.35, 0.029296875, 0.));
            add(new Tariff("Harmony L",
                    640., 24000L, 400L, 8192.,
                    0.8, 0.3, 0.0244140625, 0.));
           add(new Tariff("Stay In Touch",
                   580., 36000L, 300L, 5120.,
                   0.6, 1.5, 0.025390625, 0.));
            add(new Tariff("Insomnia",
                    580., 6000L, 100L, 30720.,
                    1., 0.3, 0.0146484375, 0.));
            add(new Tariff("PostPaid",
                    0., 0L, 0L, 0.,
                    2., 0.5, 0.0390625, 0.));
        }};
    }

    protected List<Customer> createCustomers() {
        return new ArrayList<Customer>() {{
            add(new Customer(
                    79255678912L,1345678912349091L,
                    "Dmitry", "Grigoryevich", null, "Mironov",
                    7, regionService.getRegionById(1L),
                    tariffService.getTariffById(1L),null, 145.,
                    new Date(), null, "hello123@mail.ru", null,
                    "$2a$04$CbGkvVqqINcsJkmvBeT9wOPB4ABGEgaEiLibci/CsevqQNQ3ySZ6G"
                    ));
            add(new Customer(
                    79259881285L,1200452184625959L,
                    "Alexey", "Alexandrovich", null, "Klyuev",
                    7, regionService.getRegionById(2L),
                    tariffService.getTariffById(1L),null, 130.4,
                    new Date(), null, null, null,
                    "$2a$04$CbGkvVqqINcsJkmvBeT9wOPB4ABGEgaEiLibci/CsevqQNQ3ySZ6G"
            ));
        }};
    }
}