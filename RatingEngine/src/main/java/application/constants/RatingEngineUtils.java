package application.constants;

public final class RatingEngineUtils {

    public static final String STATUS_BILLED = "BILLED";
    public static final String CURRENCY_CODE = "RUB";
    public static final String DESCRIPTION = "GOOD_BILL";
    public static final int MAX_SMS_SIZE = 256;

}
