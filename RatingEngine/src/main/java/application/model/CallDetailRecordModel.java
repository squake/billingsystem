package application.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CallDetailRecordModel {
    // Fields common for all CDRs
    private String serviceType;
    private Integer sourceCountryCode;
    private Long sourceServiceId;
    private String startDate;
    private String disposition;

    // Special fields
    private Integer destinationCountryCode; // Voice and SMS
    private Long destinationServiceId; // Voice and SMS
    private String endDate; // Voice and Traffic
    private Integer callDuration; // Voice
    private Integer roamingCode; // Voice and SMS
    private Integer smsSize; // SMS
    private String webServerName; // Traffic
    private Long trafficVolume; // Traffic
    private String trafficUnits; // Traffic
    private String trafficQuality; // Traffic
}
