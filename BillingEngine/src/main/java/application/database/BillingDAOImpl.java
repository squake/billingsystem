package application.database;

import application.entity.Bill;
import application.hibernate.HibernateUtil;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import javax.swing.*;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

@Repository
public class BillingDAOImpl implements BillingDAO {

    @Override
    public Collection<Bill> getAllBills() {
        Session session = null;
        List<Bill> bills = new LinkedList<>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            bills = session.createCriteria(Bill.class).list();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Error in 'getAllBills' method", JOptionPane.ERROR_MESSAGE);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return bills;
    }

    @Override
    public Collection<Bill> getBillsByCustomerId(Long serviceId) throws SQLException {
        Session session = null;
        List<Bill> bills = new LinkedList<>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            bills = session.createCriteria(Bill.class).list();
            bills.removeIf(p -> !p.getCustomer().getServiceId().equals(serviceId));
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Error in 'getBillsByCustomerId' method", JOptionPane.ERROR_MESSAGE);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return bills;
    }

    @Override
    public void addBill(Bill bill) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(bill);
            session.getTransaction().commit();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Error in 'addBill' method", JOptionPane.ERROR_MESSAGE);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public void update(Bill bill) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(bill);
            session.getTransaction().commit();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Error in 'addBill' method", JOptionPane.ERROR_MESSAGE);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }
}