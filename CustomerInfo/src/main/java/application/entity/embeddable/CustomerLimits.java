package application.entity.embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class CustomerLimits {

    @Column(name = "tariff_discount")
    private Double tariffDiscount;

    @Column(name = "remaining_seconds")
    private Long remainingSeconds;

    @Column(name = "remaining_sms")
    private Long remainingSMS;

    @Column(name = "remaining_megabytes")
    private Double remainingMegabytes;

}
