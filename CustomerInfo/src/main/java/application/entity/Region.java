package application.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "regions",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"country_id", "region_abbreviation"})})
public class Region {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "region_id")
    private Long regionId;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "country_id", nullable = false)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    private Country country;

    @Column(name = "region_abbreviation")
    private String regionAbbreviation;

    @Column(name = "region_name")
    private String regionName;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "region")
    private List<Customer> customers;

    public Region(Country country, String regionAbbreviation, String regionName) {
        this.country = country;
        this.regionAbbreviation = regionAbbreviation;
        this.regionName = regionName;
    }
}
