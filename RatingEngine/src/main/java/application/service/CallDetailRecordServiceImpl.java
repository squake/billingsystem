package application.service;

import application.database.CallDetailRecordFactory;
import application.entity.CallDetailRecord;
import application.entity.SMSCallDetailRecord;
import application.entity.TrafficCallDetailRecord;
import application.entity.VoiceCallDetailRecord;
import application.entity.embeddable.Roaming;
import application.enums.*;
import application.model.CallDetailRecordModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

@Service
public class CallDetailRecordServiceImpl implements CallDetailRecordService {

    @Autowired
    CustomerServiceImpl customerService;

    @Override
    public CallDetailRecord createCallDetailRecord(CallDetailRecordModel cdr) {

        if (cdr == null) {
            return null;
        }

        ServiceType serviceType;
        try {
            serviceType = getServiceTypeFrom(cdr.getServiceType());
        } catch (IllegalArgumentException e) {
            return null;
        }

        Status status = Status.RECEIVED;

        Date startDate = new Date();
        try {
            startDate = getDateFrom(cdr.getStartDate());
        } catch (IllegalArgumentException e) {
            startDate.setTime(0);
            status = Status.ERROR;
        }

        Disposition disposition = null;
        try {
            disposition = getDispositionFrom(cdr.getDisposition());
        } catch (IllegalArgumentException e) {
            status = Status.ERROR;
        }

        Integer sourceCountryCode = cdr.getSourceCountryCode();
        if (sourceCountryCode == null || sourceCountryCode < 0) {
            sourceCountryCode = null;
            status = Status.ERROR;
        }

        Long sourceServiceId = cdr.getSourceServiceId();
        if (sourceServiceId == null || sourceServiceId < 0) {
            sourceServiceId = null;
            status = Status.ERROR;
        }

        if (sourceCountryCode != null && sourceServiceId != null) {
            sourceServiceId = Long.valueOf(String.valueOf(sourceCountryCode) + String.valueOf(sourceServiceId));
        }

        if (customerService.getCustomerByServiceId(sourceServiceId) == null) {
            status = Status.ERROR;
        }

        Date endDate = new Date();
        switch (serviceType) {
            case TRAFFIC:
            case VOICE:
                try {
                    endDate = getDateFrom(cdr.getEndDate());
                } catch (IllegalArgumentException e) {
                    endDate.setTime(0);
                    status = Status.ERROR;
                }
                break;
        }

        Integer destinationCountryCode = null;
        Long destinationServiceId = null;
        switch (serviceType) {
            case VOICE:
            case SMS:
                destinationCountryCode = cdr.getDestinationCountryCode();
                if (destinationCountryCode == null || destinationCountryCode < 0) {
                    destinationCountryCode = null;
                    status = Status.ERROR;
                }
                destinationServiceId = cdr.getDestinationServiceId();
                if (destinationServiceId == null || destinationServiceId < 0) {
                    destinationServiceId = null;
                    status = Status.ERROR;
                }
                if (destinationCountryCode != null && destinationServiceId != null) {
                    destinationServiceId = Long.valueOf(String.valueOf(destinationCountryCode) + String.valueOf(destinationServiceId));
                }
                break;
        }

        CallDetailRecord callDetailRecord = null;
        switch (serviceType) {
            case TRAFFIC:
                TrafficUnits trafficUnits = null;
                try {
                    trafficUnits = getTrafficUnitsFrom(cdr.getTrafficUnits());
                } catch (IllegalArgumentException e) {
                    status = Status.ERROR;
                }
                TrafficQuality trafficQuality = null;
                try {
                    trafficQuality = getTrafficQualityFrom(cdr.getTrafficQuality());
                } catch (IllegalArgumentException e) {
                    status = Status.ERROR;
                }
                Integer coef = null;
                if (trafficUnits != null) {
                    switch (trafficUnits) {
                        case GB:
                            coef = 1024 * 1024 * 1024;
                            break;
                        case MB:
                            coef = 1024 * 1024;
                            break;
                        case KB:
                            coef = 1024;
                            break;
                        case B:
                            coef = 1;
                            break;
                    }
                }
                Long trafficVolume = cdr.getTrafficVolume();
                if (trafficVolume == null || trafficVolume < 0 || coef == null) {
                    trafficVolume = null;
                    status = Status.ERROR;
                } else {
                    trafficVolume *= coef;
                }
                String webServerName = cdr.getWebServerName();
                if (webServerName == null) {
                    status = Status.ERROR;
                }
                callDetailRecord = new TrafficCallDetailRecord(
                        status, sourceCountryCode, sourceServiceId, startDate, disposition,
                        webServerName, endDate, trafficVolume, trafficQuality
                );
                break;
            case VOICE:
                Integer callDuration = cdr.getCallDuration();
                if (callDuration == null || callDuration < 0) {
                    callDuration = null;
                    status = Status.ERROR;
                }
                Integer roamingCode = cdr.getRoamingCode();
                if (roamingCode == null || roamingCode < 0) {
                    roamingCode = null;
                    status = Status.ERROR;
                }
                callDetailRecord = new VoiceCallDetailRecord(
                        status, sourceCountryCode, sourceServiceId, startDate, disposition,
                        destinationCountryCode, destinationServiceId, endDate, callDuration, new Roaming(roamingCode)
                );
                break;
            case SMS:
                Integer smsSize = cdr.getSmsSize();
                if (smsSize == null || smsSize < 0) {
                    smsSize = null;
                    status = Status.ERROR;
                }
                callDetailRecord = new SMSCallDetailRecord(
                        status, sourceCountryCode, sourceServiceId, startDate, disposition,
                        destinationCountryCode, destinationServiceId, smsSize, new Roaming(cdr.getRoamingCode())
                );
                break;
        }

        if (!checkForUniqueness(callDetailRecord)) {
            callDetailRecord.setStatus(Status.DUPLICATED);
        }

        return callDetailRecord;
    }

    @Override
    public ServiceType getServiceTypeFrom(String serviceTypeString) throws IllegalArgumentException {
        try {
            return ServiceType.valueOf(serviceTypeString);
        } catch (IllegalArgumentException | NullPointerException e) {
            throw new IllegalArgumentException("Cannot define service type.");
        }
    }

    @Override
    public Date getDateFrom(String dateString) throws IllegalArgumentException {
        dateString = dateString.replace('T', ' ');
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss", Locale.ENGLISH);
        try {
            return formatter.parse(dateString);
        } catch (ParseException | NullPointerException e) {
            throw new IllegalArgumentException("Cannot define date.");
        }
    }

    @Override
    public Disposition getDispositionFrom(String dispositionString) throws IllegalArgumentException {
        try {
            return Disposition.valueOf(dispositionString);
        } catch (IllegalArgumentException | NullPointerException e) {
            throw new IllegalArgumentException("Cannot define disposition type.");
        }
    }

    @Override
    public TrafficUnits getTrafficUnitsFrom(String trafficUnitsString) throws IllegalArgumentException {
        try {
            return TrafficUnits.valueOf(trafficUnitsString);
        } catch (IllegalArgumentException | NullPointerException e) {
            throw new IllegalArgumentException("Cannot define unit type.");
        }
    }

    @Override
    public TrafficQuality getTrafficQualityFrom(String trafficQualityString) throws IllegalArgumentException {
        try {
            return TrafficQuality.valueOf(trafficQualityString);
        } catch (IllegalArgumentException | NullPointerException e) {
            throw new IllegalArgumentException("Cannot define service quality.");
        }
    }

    @Override
    public boolean checkForUniqueness(CallDetailRecord cdr) {
        return true;
    }

    @Override
    public Collection<CallDetailRecord> getAllCallDetailRecords() {
        try {
            return CallDetailRecordFactory.getInstance().getCallDetailRecordDAO().getAllCallDetailRecords();
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public void save(CallDetailRecord cdr) {
        if (cdr == null) {
            throw new RuntimeException("Parameter cdr of save(CallDetailRecord cdr) function is null. " +
                    "Possible cause: could not define service type.");
        }
        try {
            CallDetailRecordFactory.getInstance().getCallDetailRecordDAO().addCallDetailRecord(cdr);
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}