package application.controller;

import application.entity.Customer;
import application.entity.Payment;
import application.service.CustomerServiceImpl;
import application.service.PaymentServiceImpl;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PaymentController {

    @Autowired
    private CustomerServiceImpl customerService;
    @Autowired
    private PaymentServiceImpl paymentService;

    private JSONObject createPaymentJSON(Payment payment) {
        JSONObject resultJson = new JSONObject();
        resultJson.put("id", payment.getId());
        resultJson.put("serviceId", payment.getCustomer().getServiceId());
        resultJson.put("paymentDate", payment.getPaymentDate());
        resultJson.put("payment", payment.getPayment());
        resultJson.put("balanceAfterPayment", payment.getBalanceAfterPayment());
        return resultJson;

    }

    private JSONArray createPaymentsListJSON(List<Payment> payments) {
        JSONArray arr = new JSONArray();
        for (Payment payment : payments) {
            arr.put(createPaymentJSON(payment));
        }
        return arr;
    }

    @GetMapping(value = "/payments")
    public String getPayments(Authentication authentication) {
        Customer customer = customerService.getCustomerByServiceId(Long.parseLong(authentication.getName()));
        return createPaymentsListJSON(paymentService.getPaymentsByCustomer(customer)).toString();
    }
}
