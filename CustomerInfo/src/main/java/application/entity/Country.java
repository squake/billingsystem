package application.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@Table(name = "countries")
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "country_id")
    private Long id;

    @Column(name = "country_code", unique = true)
    private Integer countryCode;

    @Column(name = "country_abbreviation", unique = true)
    private String countryAbbreviation;

    @Column(name = "country_name", unique = true)
    private String countryName;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "country")
    private List<Region> regions;

    public Country(Integer countryCode, String countryAbbreviation, String countryName, List<Region> regions) {
        this.countryCode = countryCode;
        this.countryAbbreviation = countryAbbreviation;
        this.countryName = countryName;
        this.regions = regions;
    }
}
