package application.entity.embeddable;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Embeddable
public class Number {

    @Column(name = "country_code")
    @NonNull
    public Integer countryCode;

    @Column(name = "service_id")
    @NonNull
    public Long serviceId;


}
