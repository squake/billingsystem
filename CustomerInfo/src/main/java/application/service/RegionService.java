package application.service;

import application.entity.Customer;
import application.entity.Region;

import java.util.Collection;
import java.util.List;

public interface RegionService {
    Collection<Region> getAllRegions();

    Region getRegionById(Long regionId);

    void save(Region region);

    List<Customer> getCustomersByRegion(Region region);
}
