package application.service;

import application.database.CustomerInfoFactory;
import application.entity.Customer;
import application.entity.Region;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

@Service
public class RegionServiceImpl implements RegionService {
    @Override
    public Collection<Region> getAllRegions() {
        try {
            return CustomerInfoFactory.getInstance().getRegionDAO().getAllRegions();
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public Region getRegionById(Long regionId) {
        try {
            return CustomerInfoFactory.getInstance().getRegionDAO().getRegionById(regionId);
        } catch (SQLException e) {
           return null;
        }
    }

    @Override
    public void save(Region region) {
        try {
            CustomerInfoFactory.getInstance().getRegionDAO().addRegion(region);
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public List<Customer> getCustomersByRegion(Region region) {
        return region.getCustomers();
    }
}
