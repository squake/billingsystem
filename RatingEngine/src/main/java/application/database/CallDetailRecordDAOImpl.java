package application.database;

import application.entity.CallDetailRecord;
import application.hibernate.HibernateUtil;
import org.hibernate.Session;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class CallDetailRecordDAOImpl implements CallDetailRecordDAO {
    @Override
    public void addCallDetailRecord(CallDetailRecord cdr) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(cdr);
            session.getTransaction().commit();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public Collection<CallDetailRecord> getAllCallDetailRecords() {
        Session session = null;
        List<CallDetailRecord> cdrs = new LinkedList<>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            cdrs = session.createCriteria(CallDetailRecord.class).list();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return cdrs;
    }
}
