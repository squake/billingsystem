package application.entity;

import application.entity.embeddable.CustomerLimits;
import application.entity.embeddable.CustomerName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"region", "tariff", "password"})
@EqualsAndHashCode(of = {"personalAccount"})
@Table(name = "customers")
public class Customer {

    @Id
    @Column(name = "service_id")
    @NonNull
    private Long serviceId;

    @Column(name = "personal_account", unique = true)
    @NonNull
    private Long personalAccount;

    @AttributeOverrides({
            @AttributeOverride(name = "firstName", column = @Column(name = "first_name")),
            @AttributeOverride(name = "middleName", column = @Column(name = "middle_name")),
            @AttributeOverride(name = "patronymic", column = @Column(name = "patronymic")),
            @AttributeOverride(name = "lastName", column = @Column(name = "last_name"))
    })
    private CustomerName name;

    @Column(name = "country_code")
    @NonNull
    private Integer countryCode;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "region_id", nullable = false)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    private Region region;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "tariff_id", nullable = false)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    private Tariff tariff;

    @AttributeOverrides({
            @AttributeOverride(name = "remainingSeconds", column = @Column(name = "remaining_seconds")),
            @AttributeOverride(name = "remainingSMS", column = @Column(name = "remaining_sms")),
            @AttributeOverride(name = "remainingMegabytes", column = @Column(name = "remaining_megabytes")),
    })
    private CustomerLimits limits;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "customer")
    private List<Payment> payments;

    @Column(name = "balance")
    @NonNull
    private Double balance;

    @Column(name = "creation_date")
    @CreationTimestamp
    private Date creationDate;

    @Column(name = "birthday")
    private Date birthday;

    @Column(name = "email")
    private String email;

    @Column(name = "additional_service_id")
    private Long additionalServiceId;

    @Column(name = "password")
    @JsonIgnore
    @NonNull
    private String password;

    public String getFirstName() {
        return name.getFirstName();
    }

    public String getPatronymic() {
        return name.getPatronymic();
    }

    public String getMiddleName() {
        return name.getMiddleName();
    }

    public String getLastName() {
        return name.getLastName();
    }

    public Long getRemainingSeconds() {
        return limits.getRemainingSeconds();
    }

    public Long getRemainingSMS() {
        return limits.getRemainingSMS();
    }

    public Double getRemainingMegabytes() {
        return limits.getRemainingMegabytes();
    }

    public void setRemainingMegabytes(Double traffic) {
        limits.setRemainingMegabytes(traffic);
    }

    public void setRemainingSeconds(Long seconds) {
        limits.setRemainingSeconds(seconds);
    }

    public void setRemainingSMS(Long sms) {
        limits.setRemainingSMS(sms);
    }


    public Customer(@NonNull Long serviceId, @NonNull Long personalAccount, String firstName, String patronymic, String middleName, String lastName, @NonNull Integer countryCode, Region region, Tariff tariff, List<Payment> payments, @NonNull Double balance, Date creationDate, Date birthday, String email, Long additionalServiceId, @NonNull String password) {
        this(serviceId, personalAccount, new CustomerName(firstName, patronymic, middleName, lastName), countryCode, region, tariff, new CustomerLimits(tariff.getDiscount(), tariff.getFreeSeconds(), tariff.getFreeSMS(), tariff.getFreeMegabytes()), payments, balance, creationDate, birthday, email, additionalServiceId, password);
    }
}