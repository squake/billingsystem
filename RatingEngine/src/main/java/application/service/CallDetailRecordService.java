package application.service;

import application.entity.CallDetailRecord;
import application.enums.Disposition;
import application.enums.TrafficQuality;
import application.enums.ServiceType;
import application.enums.TrafficUnits;
import application.model.CallDetailRecordModel;

import java.util.Collection;
import java.util.Date;

public interface CallDetailRecordService {

    CallDetailRecord createCallDetailRecord(CallDetailRecordModel cdr);

    ServiceType getServiceTypeFrom(String serviceTypeString) throws IllegalArgumentException;

    Date getDateFrom(String dateString) throws IllegalArgumentException;

    Disposition getDispositionFrom(String dispositionString) throws IllegalArgumentException;

    TrafficUnits getTrafficUnitsFrom(String trafficUnitsString) throws IllegalArgumentException;

    TrafficQuality getTrafficQualityFrom(String trafficQualityString) throws IllegalArgumentException;

    boolean checkForUniqueness(CallDetailRecord cdr);

    Collection<CallDetailRecord> getAllCallDetailRecords();

    void save(CallDetailRecord cdr);

}
