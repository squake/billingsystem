package application.service;

import application.entity.Customer;
import application.entity.embeddable.CustomerLimits;
import application.entity.embeddable.CustomerName;
import application.model.CustomerModel;

import java.util.Collection;

public interface CustomerService {

    Collection<Customer> getAllCustomers();

    CustomerModel createCustomerModel(Long serviceId);

    void updateCustomer(Customer customer);

    Customer getCustomerByServiceId(Long serviceId);

    CustomerName getCustomerNameByServiceId(Long serviceId);

    void save(Customer customer);

    Double getBalance(Long serviceId);

    CustomerLimits getLimits(Long serviceId);

    String updateBalance(Customer customer, Double paymentValue);

    String updateCustomerTariff(Customer customer, String newTariffName);

    String updateCustomer(Customer customer, String email, Long additionalServiceId);

}
