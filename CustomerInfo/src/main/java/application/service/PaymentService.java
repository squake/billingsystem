package application.service;

import application.entity.Customer;
import application.entity.Payment;

import java.util.Collection;

public interface PaymentService {

    Collection<Payment> getAllPayments();

    void save(Payment payment);

    Collection<Payment> getPaymentsByCustomer(Customer customer);

}

