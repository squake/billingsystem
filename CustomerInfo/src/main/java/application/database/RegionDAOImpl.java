package application.database;

import application.entity.Region;
import application.hibernate.HibernateUtil;
import org.hibernate.Session;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class RegionDAOImpl implements RegionDAO {

    @Override
    public void addRegion(Region region) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(region);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public Region getRegionById(Long regionId) {
        Session session = null;
        Region region = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            region = session.load(Region.class, regionId);
        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return region;
    }

    @Override
    public Collection<Region> getAllRegions() {
        Session session = null;
        List<Region> regions = new LinkedList<>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            regions = session.createCriteria(Region.class).list();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return regions;
    }
}
