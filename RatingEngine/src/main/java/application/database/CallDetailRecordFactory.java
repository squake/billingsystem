package application.database;

public class CallDetailRecordFactory {

    private static CallDetailRecordDAO callDetailRecordDAO = null;
    private static CallDetailRecordFactory instance = null;

    public static synchronized CallDetailRecordFactory getInstance() {
        if (instance == null) {
            instance = new CallDetailRecordFactory();
        }
        return instance;
    }

    public CallDetailRecordDAO getCallDetailRecordDAO() {
        if (callDetailRecordDAO == null) {
            callDetailRecordDAO = new CallDetailRecordDAOImpl();
        }
        return callDetailRecordDAO;
    }
}
