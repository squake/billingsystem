package application.service;

import application.database.BillingFactory;
import application.entity.Bill;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.Collection;

@Service
public class BillingServiceImpl implements BillingService {

    @Override
    public Collection<Bill> getAllBills() {
        try {
            return BillingFactory.getInstance().getBillingDAO().getAllBills();
        } catch (SQLException exc) {
            return null;
        }
    }

    @Override
    public Collection<Bill> getBillsByCustomerId(Long serviceId) {
        try {
            return BillingFactory.getInstance().getBillingDAO().getBillsByCustomerId(serviceId);
        } catch (SQLException exc) {
            return null;
        }
    }

    @Override
    public void save(Bill bill) {
        try {
            BillingFactory.getInstance().getBillingDAO().addBill(bill);
        } catch (SQLException exc) {
            throw new RuntimeException(exc.getMessage(), exc);
        }
    }

    @Override
    public void update(Bill bill) {
        try {
            BillingFactory.getInstance().getBillingDAO().update(bill);
        } catch (SQLException exc) {
            throw new RuntimeException(exc.getMessage(), exc);
        }
    }
}