package application.database;

import application.entity.Tariff;
import application.hibernate.HibernateUtil;
import org.hibernate.Session;

import java.util.List;

public class TariffDAOImpl implements TariffDAO {
    @Override
    public void addTariff(Tariff tariff) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(tariff);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public Tariff getTariffById(Long tariffId) {
        Session session = null;
        Tariff tariff = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            List<Tariff> list = session.createSQLQuery("select t.* from tariffs as t where t.tariff_id = " + tariffId)
                    .addEntity(Tariff.class).list();
            if (list.size() == 1) {
                tariff = list.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return tariff;
    }

    @Override
    public Tariff getTariffByName(String tariffName) {
        Session session = null;
        Tariff tariff = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            List<Tariff> list = session.createSQLQuery("select t.* from tariffs as t where t.name = '" + tariffName + "'")
                    .addEntity(Tariff.class).list();
            if (list.size() >= 1) {
                tariff = list.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return tariff;
    }


    @Override
    public List<Tariff> getAllTariffs() {
        Session session = null;
        List<Tariff> tariffs = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tariffs = session.createSQLQuery("select t.* from tariffs as t")
                    .addEntity(Tariff.class)
                    .list();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return tariffs;
    }

    @Override
    public List<Tariff> getAvailableTariffs() {
        Session session = null;
        List<Tariff> tariffs = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tariffs = session.createSQLQuery("select t.* from tariffs as t where t.available = true")
                    .addEntity(Tariff.class)
                    .list();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return tariffs;
    }
}
