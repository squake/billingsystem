package application.model;

import application.entity.embeddable.CustomerLimits;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomerModel {
    private Long serviceId;
    private Long personalAccount;
    private String firstName;
    private String patronymic;
    private String middleName;
    private String lastName;
    private String region;
    private Long tariffId;
    private CustomerLimits limits;
    private Date creationDate;
    private Date birthday;
    private String email;
    private Long additionalServiceId;
}
