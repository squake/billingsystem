package application.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer {

    static final String END_POINT = "/notifications";

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        if (registry != null) {
            // Enable a broker to send messages to the client on destinations prefixed with '/topic'
            registry.enableSimpleBroker("/topic", "/queue", "/chat");
            // prefix for messages that are bound for @MessageMapping annotated methods. This prefix will be used to define all message mappings
            registry.setApplicationDestinationPrefixes("/app");
        }
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        // This code register the '/notifications' end-point. The SockJS client will attempt to connect to '/notifications' end-point
        if (registry != null) {
            registry.addEndpoint(END_POINT).setAllowedOrigins("*").withSockJS();
        }
    }

}
