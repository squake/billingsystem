package application.initialization;

import application.entity.*;
import application.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Component
public class InitDataBases implements ApplicationRunner {

    @Autowired
    private BillingServiceImpl billingService;
    @Autowired
    private CustomerServiceImpl customerService;
    @Autowired
    private PaymentServiceImpl paymentService;
    @Autowired
    private CountryServiceImpl countryService;
    @Autowired
    private RegionServiceImpl regionService;
    @Autowired
    private TariffServiceImpl tariffService;
    @Autowired
    private CallDetailRecordServiceImpl callDetailRecordService;
    @Autowired
    private CustomerInfoUtils customerInfoUtils;
    @Autowired
    private RatingEngineUtils ratingEngineUtils;
    @Autowired
    private BillingEngineUtils billingEngineUtils;

    @Override
    public void run(ApplicationArguments args) {
        addCountriesInDataBase();
        addRegionsInDataBase();
        addTariffsInDataBase();
        addCustomersInDataBase();
        addBillsInDataBase();
        addCallDetailRecordsInDataBase();
        addPaymentsInDataBase();

        Collection<Bill> bills = billingService.getAllBills();
        long currentTime = System.currentTimeMillis();
        for (Bill bill : bills) {
            bill.setCreationDate(new Date(currentTime -= 86400000L));
            billingService.update(bill);
        }
    }

    private void addCountriesInDataBase() {
        List<Country> countries = customerInfoUtils.createCountries();
        countries.forEach(country -> countryService.save(country));
    }

    private void addRegionsInDataBase() {
        List<Region> regions = customerInfoUtils.createRegions();
        regions.forEach(region -> regionService.save(region));
    }

    private void addCustomersInDataBase() {
        List<Customer> customers = customerInfoUtils.createCustomers();
        customers.forEach(customer -> customerService.save(customer));
    }

    private void addTariffsInDataBase() {
        List<Tariff> tariffs = customerInfoUtils.createTariffs();
        tariffs.forEach(tariff -> tariffService.save(tariff));
    }

    private void addBillsInDataBase() {
        List<Customer> customers = new ArrayList<>(customerService.getAllCustomers());
        List<Bill> bills = billingEngineUtils.createBills();

        bills.get(0).setCustomer(customers.get(0));
        bills.get(1).setCustomer(customers.get(0));
        bills.get(2).setCustomer(customers.get(0));
        bills.get(3).setCustomer(customers.get(1));
        bills.get(4).setCustomer(customers.get(1));
        bills.get(5).setCustomer(customers.get(1));

        bills.forEach(bill -> billingService.save(bill));
    }

    private void addCallDetailRecordsInDataBase() {
        List<CallDetailRecord> cdrs = ratingEngineUtils.createCallDetailRecords();
        cdrs.forEach(cdr -> callDetailRecordService.save(cdr));
    }

    private void addPaymentsInDataBase() {
        List<Payment> payments = customerInfoUtils.createPayments();
        payments.forEach(payment -> paymentService.save(payment));
    }
}