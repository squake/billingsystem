package application.database;

import application.entity.Customer;
import application.entity.Payment;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface PaymentDAO {

    void addPayment(Payment payment) throws SQLException;

    Payment getPaymentById(Long payment_id) throws SQLException;

    List<Payment> getPaymentsByCustomer(Customer customer) throws SQLException;

    Collection<Payment> getAllPayments() throws SQLException;

    void deletePayment(Payment payment) throws SQLException;
}



