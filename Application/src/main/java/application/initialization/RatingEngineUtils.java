package application.initialization;

import application.entity.CallDetailRecord;
import application.entity.SMSCallDetailRecord;
import application.entity.TrafficCallDetailRecord;
import application.entity.VoiceCallDetailRecord;
import application.entity.embeddable.Roaming;
import application.enums.Disposition;
import application.enums.Status;
import application.enums.TrafficQuality;
import application.service.CustomerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class RatingEngineUtils {
    @Autowired
    CustomerServiceImpl customerService;

    protected List<CallDetailRecord> createCallDetailRecords() {
        return new ArrayList<CallDetailRecord>() {{
            add(new VoiceCallDetailRecord(
                    Status.RECEIVED, 7, 79255678912L,
                    new Date(), Disposition.SUCCESS,
                    7, 9999999L, new Date(), 44, new Roaming(5)));
            add(new SMSCallDetailRecord(
                    Status.RECEIVED, 7, 79255678912L,
                    new Date(), Disposition.SUCCESS,
                    7, 9563452411L, 12, new Roaming(5)));
            add(new TrafficCallDetailRecord(
                    Status.RECEIVED, 7, 79255678912L,
                    new Date(), Disposition.SUCCESS,
                    "some web server", new Date(), 10023L, TrafficQuality.SPEED));
            add(new TrafficCallDetailRecord(
                    Status.RECEIVED, 7, 79255678912L,
                    new Date(), Disposition.SUCCESS,
                    "some web server", new Date(), 3141L, TrafficQuality.SPEED));
        }};
    }
}