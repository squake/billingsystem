package application.enums;

public enum Disposition {
    SUCCESS, UNREACHABLE, ABORTED
}
