package application.model;

import application.enums.ServiceType;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Notification {
    private Long serviceId;
    private double total;
    private ServiceType serviceType;
    private Long remainingValue;
}
