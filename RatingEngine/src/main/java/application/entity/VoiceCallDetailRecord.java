package application.entity;

import application.entity.embeddable.Roaming;
import application.enums.Disposition;
import application.enums.Status;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@DiscriminatorValue(value = "Voice")
public class VoiceCallDetailRecord extends CallDetailRecord {

    @Column(name = "destination_country_code")
    private Integer destinationCountryCode;

    @Column(name = "destination_service_id")
    private Long destinationServiceId;

    @Column(name = "end_date")
    private Date endDate;

    @Column(name = "call_duration")
    private Integer callDuration;

    @AttributeOverride(name = "roamingCode", column = @Column(name = "roaming_code"))
    private Roaming roaming;

    public VoiceCallDetailRecord(Status status, Integer sourceCountryCode, Long sourceServiceId, Date startDate, Disposition disposition, Integer destinationCountryCode, Long destinationServiceId, Date endDate, Integer callDuration, Roaming roaming) {
        super(status, sourceCountryCode, sourceServiceId, startDate, disposition);
        this.destinationCountryCode = destinationCountryCode;
        this.destinationServiceId = destinationServiceId;
        this.endDate = endDate;
        this.callDuration = callDuration;
        this.roaming = roaming;
    }
}
