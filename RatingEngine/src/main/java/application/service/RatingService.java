package application.service;


import application.entity.CallDetailRecord;
import application.model.Notification;

public interface RatingService {
    Notification calculateAndGetNotificationToSendUser(CallDetailRecord cdr);
}
