package application.initialization;

import application.entity.Bill;
import application.enums.ServiceType;
import application.service.CustomerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class BillingEngineUtils {
    @Autowired
    CustomerServiceImpl customerService;

    protected List<Bill> createBills() {
        return new ArrayList<Bill>() {{
            add(new Bill("Total Customer Bill", new Date(), "USD", "Billed",
                    50., 10., 40., ServiceType.SMS,
                    customerService.getCustomerByServiceId(79255678912L)));
            add(new Bill("Total Customer Bill", new Date(), "USD", "Billed",
                    34., 12., 22., ServiceType.SMS,
                    customerService.getCustomerByServiceId(79255678912L)));
            add(new Bill("Total Customer Bill", new Date(), "USD", "Billed",
                    10., 0., 10., ServiceType.VOICE,
                    customerService.getCustomerByServiceId(79255678912L)));
            add(new Bill("Total Customer Bill", new Date(), "USD", "Billed",
                    15., 0., 15., ServiceType.VOICE,
                    customerService.getCustomerByServiceId(79255678912L)));
            add(new Bill("Total Customer Bill", new Date(), "USD", "Billed",
                    120., 10., 110., ServiceType.TRAFFIC,
                    customerService.getCustomerByServiceId(79255678912L)));
            add(new Bill("Total Customer Bill", new Date(), "USD", "Billed",
                    50., 10., 40., ServiceType.SMS,
                    customerService.getCustomerByServiceId(79255678912L)));
            add(new Bill("Total Customer Bill", new Date(), "USD", "Billed",
                    22., 10., 12., ServiceType.VOICE,
                    customerService.getCustomerByServiceId(79255678912L)));
            add(new Bill("Total Customer Bill", new Date(), "USD", "Billed",
                    100., 10., 90., ServiceType.TRAFFIC,
                    customerService.getCustomerByServiceId(79255678912L)));
            add(new Bill("Total Customer Bill", new Date(), "USD", "Billed",
                    0., 0., 0., ServiceType.VOICE,
                    customerService.getCustomerByServiceId(79259881285L)));
            add(new Bill("Total Customer Bill", new Date(), "USD", "Billed",
                    2., 0.4, 1.6, ServiceType.TRAFFIC,
                    customerService.getCustomerByServiceId(79259881285L)));
            add(new Bill("Total Customer Bill", new Date(), "USD", "Billed",
                    34., 10.5, 23.5, ServiceType.TRAFFIC,
                    customerService.getCustomerByServiceId(79259881285L)));
        }};
    }
}