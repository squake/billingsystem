package application.database;

import application.entity.CallDetailRecord;

import java.sql.SQLException;
import java.util.Collection;

public interface CallDetailRecordDAO {
    void addCallDetailRecord(CallDetailRecord cdr) throws SQLException;

    Collection<CallDetailRecord> getAllCallDetailRecords() throws SQLException;

}
