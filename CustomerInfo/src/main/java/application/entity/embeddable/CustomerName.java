package application.entity.embeddable;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class CustomerName {

    @Column(name = "first_name")
    @NonNull
    private String firstName;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "patronymic")
    private String patronymic;

    @Column(name = "last_name")
    @NonNull
    private String lastName;

}

