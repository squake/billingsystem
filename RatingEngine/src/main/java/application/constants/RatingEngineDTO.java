package application.constants;

import application.entity.Customer;
import application.entity.Tariff;
import lombok.Data;

import java.util.Date;

@Data
public class RatingEngineDTO {

    private Customer customer;
    private Tariff tariff;
    private Date creationDate;
    private Double discount;
    private Double trafficInMB;
    private Double remainingTraffic;
    private Integer callDuration;
    private Long remainingSeconds;
    private Integer numberOfSMS;
    private Long remainingSMS;
    private Double total;
    private Double subtotal;
    private Long sourceServiceId;

    public RatingEngineDTO(Customer customer, Date creationDate, Double trafficInMB, Integer callDuration,
                           Integer numberOfSMS, Double total, Double subtotal, Long sourceServiceId) {
        this.customer = customer;
        this.tariff = this.customer.getTariff();
        this.creationDate = creationDate;
        this.discount = this.tariff.getDiscount();
        this.trafficInMB = trafficInMB;
        this.remainingTraffic = this.customer.getRemainingMegabytes();
        this.callDuration = callDuration;
        this.remainingSeconds = this.customer.getRemainingSeconds();
        this.numberOfSMS = numberOfSMS;
        this.remainingSMS = this.customer.getRemainingSMS();
        this.total = total;
        this.subtotal = subtotal;
        this.sourceServiceId = sourceServiceId;
    }
}
