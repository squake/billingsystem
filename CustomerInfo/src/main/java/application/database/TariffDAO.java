package application.database;

import application.entity.Tariff;

import java.sql.SQLException;
import java.util.List;

public interface TariffDAO {
    void addTariff(Tariff tariff) throws SQLException;

    Tariff getTariffById(Long tariffId) throws SQLException;

    Tariff getTariffByName(String tariffName) throws SQLException;

    List<Tariff> getAllTariffs();

    List<Tariff> getAvailableTariffs();
}