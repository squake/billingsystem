package application.database;

import application.entity.Country;
import application.hibernate.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class CountryDAOImpl implements CountryDAO {

    @Override
    public void addCountry(Country country) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(country);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public Country getCountryById(Long countryId) {
        Session session = null;
        Country country = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            country = session.load(Country.class, countryId);
        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return country;
    }

    @Override
    public Country getCountryByCountryCode(Integer countryCode) {
        Session session = null;
        Country country = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(Country.class);
            criteria.add(Restrictions.eq("countryCode", countryCode));
            country = (Country) criteria.uniqueResult();
        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return country;
    }

    @Override
    public Collection<Country> getAllCountries() {
        Session session = null;
        List<Country> countries = new LinkedList<>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            countries = session.createCriteria(Country.class).list();
        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return countries;
    }
}
