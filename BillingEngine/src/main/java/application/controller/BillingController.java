package application.controller;

import application.entity.Bill;
import application.enums.ServiceType;
import application.service.BillingService;
import application.service.CustomerService;
import application.util.GeneratePdfReport;
import com.itextpdf.text.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.Principal;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/billing")
public class BillingController {

    @Autowired
    BillingService billingService;

    @Autowired
    CustomerService customerService;

    @GetMapping("/bills")
    public Collection<Bill> getAllBills(Principal principal) {
        return billingService.getBillsByCustomerId(Long.parseLong(principal.getName()));
    }

    @GetMapping(value = "/invoice", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> getBill(Principal principal, @RequestParam("date_from") String dateFrom, @RequestParam("date_to") String dateTo) throws IOException {
        List<Bill> bills = (List<Bill>) billingService.getBillsByCustomerId(Long.parseLong(principal.getName()));

        bills.removeIf(bill -> bill.getCreationDate().getTime() < Long.parseLong(dateFrom)
                || bill.getCreationDate().getTime() > (Long.parseLong(dateTo) + 86400000L));

        ByteArrayInputStream bis = null;
        try {
            bis = GeneratePdfReport.getInvoice(customerService.getCustomerByServiceId(Long.parseLong(principal.getName())),
                    bills, dateFrom, dateTo);
        } catch (DocumentException e) {
            throw new RuntimeException("An exception occurred while creating PDF invoice", e);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=invoice.pdf");

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }

    @GetMapping(value = "/detalization", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> getFullPdfReport(Principal principal, @RequestParam("service_type") String serviceType, @RequestParam("date_from") String dateFrom, @RequestParam("date_to") String dateTo) throws IOException {
        List<Bill> bills = (List<Bill>) billingService.getBillsByCustomerId(Long.parseLong(principal.getName()));
        bills.removeIf(p -> !p.getServiceType().equals(ServiceType.valueOf(serviceType.toUpperCase())));

        ByteArrayInputStream bis = null;
        try {
            bis = GeneratePdfReport.getFullPdfReport(
                    customerService.getCustomerByServiceId(Long.parseLong(principal.getName())), bills, dateFrom, dateTo, serviceType);
        } catch (DocumentException e) {
            throw new RuntimeException("An exception occurred while creating PDF detalization", e);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=detalization.pdf");

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }
}