package application.entity;

import application.enums.Disposition;
import application.enums.Status;
import application.enums.TrafficQuality;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@DiscriminatorValue(value = "Traffic")
public class TrafficCallDetailRecord extends CallDetailRecord {

    @Column(name = "web_server_name")
    private String webServerName;

    @Column(name = "end_date")
    private Date endDate;

    @Column(name = "traffic_volume")
    private Long trafficVolume;

    @Enumerated(EnumType.STRING)
    @Column(name = "traffic_quality", length = 16)
    private TrafficQuality trafficQuality;

    public TrafficCallDetailRecord(Status status, Integer sourceCountryCode, Long sourceServiceId, Date startDate, Disposition disposition, String webServerName, Date endDate, Long trafficVolume, TrafficQuality trafficQuality) {
        super(status, sourceCountryCode, sourceServiceId, startDate, disposition);
        this.webServerName = webServerName;
        this.endDate = endDate;
        this.trafficVolume = trafficVolume;
        this.trafficQuality = trafficQuality;
    }
}
