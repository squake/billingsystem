package application.entity;

import application.entity.embeddable.Roaming;
import application.enums.Disposition;
import application.enums.Status;
import lombok.*;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;

@Data
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@DiscriminatorValue(value = "SMS")
public class SMSCallDetailRecord extends CallDetailRecord {

    @Column(name = "destination_country_code")
    private Integer destinationCountryCode;

    @Column(name = "destination_service_id")
    private Long destinationServiceId;

    @Column(name = "sms_size")
    private Integer smsSize;

    @AttributeOverride(name = "roamingCode", column = @Column(name = "roaming_code"))
    private Roaming roaming;

    public SMSCallDetailRecord(Status status, Integer sourceCountryCode, Long sourceServiceId, Date startDate, Disposition disposition, Integer destinationCountryCode, Long destinationServiceId, Integer smsSize, Roaming roaming) {
        super(status, sourceCountryCode, sourceServiceId, startDate, disposition);
        this.destinationCountryCode = destinationCountryCode;
        this.destinationServiceId = destinationServiceId;
        this.smsSize = smsSize;
        this.roaming = roaming;
    }

}
